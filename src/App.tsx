import React, { useEffect, useState } from 'react';
import 'bootswatch/dist/darkly/bootstrap.min.css';
import './delivery/styles/main.scss';
import './App.css';
import { LocationCards } from './delivery/components/locations/LocationCards';
import { context, Context, useAppContext } from './dependency-injection/context';
import { ZombieList } from './delivery/components/zombies/ZombieList';
import { Location } from './core/locations/domain/Location';
import { Spinner } from 'react-bootstrap';

function App() {
  const { actions } = useAppContext();
  const [locations, setLocations] = useState<Location[]>([]);
  const [updateLocations, setUpdateLocations] = useState(false);
  const [selectedLocation, setSelectedLocation] = useState<Location>();
  const [showLoading, setShowLoading] = useState(false);

  const loadingAppContext = {
    ...context,
    showLoading: (val: boolean) => {
      setShowLoading(val);
    },
  };

  const onZombieListUpdate = () => {
    setUpdateLocations(!updateLocations);
  }

  useEffect(() => {
    const fetchLocations = async () => {
      setShowLoading(true);
      const getLocationsAction = actions.forGetLocations();
      const data = await getLocationsAction.execute();
      setLocations((_) => data);
      setShowLoading(false);
    }
    fetchLocations();
  }, [updateLocations, actions]);

  const onLocationSelected = (location: Location) => {
    setSelectedLocation(location);
  }

  return (
    <Context.Provider value={loadingAppContext}>
      <div className="App">

        <div className="container">

          <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <span className="navbar-brand">Zombie Manager</span>
          </nav>

          {showLoading && (
            <div id="overlay">
              <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
              </Spinner>
            </div>
          )}

          <LocationCards onClick={onLocationSelected} locations={locations} />
          <ZombieList clearSelectedLocation={() => { setSelectedLocation(undefined) }} selectedLocation={selectedLocation} locations={locations} onSaved={onZombieListUpdate} />
        </div>

      </div>
    </Context.Provider>
  );
}

export default App;
