import { HTTPService } from "../../common/domain/HTTPService";
import { APIConfig } from "../../common/infrastructure/APIConfig";
import { Location } from "../domain/Location";
import { LocationsRepository } from "../domain/LocationsRepository";
import { LocationsResponse } from "./LocationsResponse";

export class HTTPLocationsRepository implements LocationsRepository {
  constructor(private httpService: HTTPService, private apiConfig: APIConfig) { }

  async find(): Promise<Location[]> {
    const url = `${this.apiConfig.url}/locations`;
    const response = await this.httpService.get<LocationsResponse>(url);

    return response.data;
  }
}
