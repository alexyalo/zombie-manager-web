import { Location } from "../domain/Location";

export interface LocationsResponse {
  data: Location[];
}
