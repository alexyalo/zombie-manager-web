import Substitute from "@fluffy-spoon/substitute";
import { HTTPService } from "../../../common/domain/HTTPService";
import { Location } from "../../domain/Location";
import { HTTPLocationsRepository } from "../HTTPLocationsRepository";
import { LocationsResponse } from "../LocationsResponse";

const fakeLocationsResponse: LocationsResponse = {
  data: [{
    id: 'test1',
    name: 'Hospital',
    zombieCount: 10,
  }]
};

const fakeLocations: Location[] = [{
  id: 'test1',
  name: 'Hospital',
  zombieCount: 10,
}];

const fakeURL = 'http://localhost';

describe('HTTPLocationsRepository Unit Test', () => {
  describe('find method', () => {
    it('should invoke HTTPService with the right parameters and parse the results to domain objects', async () => {
      const httpService = Substitute.for<HTTPService>();
      httpService.get(`${fakeURL}/locations`).resolves(fakeLocationsResponse);
      const repo = new HTTPLocationsRepository(httpService, { url: fakeURL });

      const locations = await repo.find();

      expect(locations).toStrictEqual(fakeLocations);
    });
  });
});
