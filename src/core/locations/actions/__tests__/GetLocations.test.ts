import Substitute from '@fluffy-spoon/substitute';
import { Location } from '../../domain/Location';
import { LocationsRepository } from "../../domain/LocationsRepository";
import { GetLocations } from "../GetLocations";

const fakeLocations: Location[] = [{
  id: 'test1',
  name: 'Hospital',
  zombieCount: 10,
}, {
  id: 'test2',
  name: 'School',
  zombieCount: 32,
}, {
  id: 'test3',
  name: 'Warehouse',
  zombieCount: 16,
}];

describe('GetLocations Action Unit Test', () => {
  it('should invoke the repo and return the results', async () => {
    const repo = Substitute.for<LocationsRepository>();
    repo.find().resolves(fakeLocations);
    const getLocations = new GetLocations(repo);

    const locations = await getLocations.execute();

    repo.received(1).find();
    expect(locations).toStrictEqual(fakeLocations);
  });
});
