import { Location } from "../domain/Location";
import { LocationsRepository } from "../domain/LocationsRepository";

export class GetLocations {
  constructor(private locationsRepo: LocationsRepository) { }

  async execute(): Promise<Location[]> {
    return this.locationsRepo.find();
  }
}