import { FindRepository } from "../../common/domain/repositories/FindRepository";
import { Location } from "./Location";

type Locations = Location[];

export type LocationsRepository = FindRepository<Locations>;