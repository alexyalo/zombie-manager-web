export interface Location {
  id: string;
  name: string;
  zombieCount: number;
}