import axios from 'axios';

import { HTTPError } from '../domain/HTTPError';
import { HTTPService } from '../domain/HTTPService';

export class AxiosHTTPService implements HTTPService {
  public async delete(url: string): Promise<void> {
    const res = await axios.delete(url, this.getRequestConfig());

    if (res.status > 204) {
      throw new HTTPError();
    }
  }

  public async post<I, T>(url: string, data: I): Promise<T> {
    const result = await axios.post(url, data, this.getRequestConfig());

    if (result.status > 204) {
      throw new HTTPError();
    }

    return result.data as T;
  }

  public async patch<I, T>(url: string, data: I): Promise<T> {
    const result = await axios.patch(url, data, this.getRequestConfig());

    if (result.status > 204) {
      throw new HTTPError();
    }

    return result.data as T;
  }

  public async get<T>(url: string): Promise<T> {
    const result = await axios.get(url, this.getRequestConfig());

    if (result.status !== 200) {
      throw new HTTPError();
    }

    return result.data as T;
  }

  private getHeaders() {
    const jwt = localStorage.getItem('jwt');
    return {
      Authorization: jwt ? `Bearer ${jwt}` : '',
    };
  }

  private getRequestConfig() {
    return {
      headers: this.getHeaders(),
    };
  }
}
