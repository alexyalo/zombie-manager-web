export interface HTTPService {
  post<I, T>(url: string, data: I): Promise<T>;
  patch<I, T>(url: string, data: I): Promise<T>;
  get<T>(url: string): Promise<T>;
  delete(url: string): Promise<void>;
}
