export interface CreateRepository<T, I> {
  create(input: I): Promise<T>;
}
