export interface FindRepository<T, P = any> {
  find(): Promise<T>;
  find(params: P): Promise<T>;
}
