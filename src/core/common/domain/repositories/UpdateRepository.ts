export interface UpdateRepository<T, I> {
  update(id: string, input: I): Promise<T>;
}
