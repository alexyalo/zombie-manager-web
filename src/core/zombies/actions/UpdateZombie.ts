import { Zombie } from "../domain/Zombie";
import { ZombieInput } from "../domain/ZombieInput";
import { ZombiesRepository } from "../domain/ZombiesRepository";

export class UpdateZombie {
  constructor(private zombiesRepo: ZombiesRepository) { }

  execute(id: string, input: ZombieInput): Promise<Zombie> {
    return this.zombiesRepo.update(id, input);
  }
}