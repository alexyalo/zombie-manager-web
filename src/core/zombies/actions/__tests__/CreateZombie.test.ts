import Substitute from '@fluffy-spoon/substitute';
import { ZombieInput } from '../../domain/ZombieInput';
import { Zombie } from '../../domain/Zombie';
import { ZombiesRepository } from '../../domain/ZombiesRepository';
import { CreateZombie } from '../CreateZombie';

const fakeZombie: Zombie = {
  id: 'test-id',
  name: 'John Doe',
  location: {
    id: 'test-id',
    name: 'Hospital'
  }
};
const input: ZombieInput = {
  name: 'Test name',
  locationId: 'Test Location Id',
}

describe('CreateZombie Action Unit Test', () => {
  it('should invoke the repo with the right parameters and return the result', async () => {
    const repo = Substitute.for<ZombiesRepository>();
    repo.create(input).resolves(fakeZombie);
    const createZombie = new CreateZombie(repo);

    const zombies = await createZombie.execute(input);

    repo.received(1).create(input);
    expect(zombies).toStrictEqual(fakeZombie);
  });
});
