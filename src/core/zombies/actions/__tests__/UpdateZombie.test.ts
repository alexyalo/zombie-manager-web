import Substitute from '@fluffy-spoon/substitute';
import { ZombieInput } from '../../domain/ZombieInput';
import { Zombie } from '../../domain/Zombie';
import { ZombiesRepository } from '../../domain/ZombiesRepository';
import { UpdateZombie } from '../UpdateZombie';

const fakeZombie: Zombie = {
  id: 'test-id',
  name: 'John Doe',
  location: {
    id: 'test-id',
    name: 'Hospital'
  }
};
const input: ZombieInput = {
  name: 'Test name',
  locationId: 'Test Location Id',
}

describe('UpdateZombie Action Unit Test', () => {
  it('should invoke the repo with the right parameters and return the result', async () => {
    const repo = Substitute.for<ZombiesRepository>();
    const id = 'test-id';
    repo.update(id, input).resolves(fakeZombie);
    const updateZombie = new UpdateZombie(repo);

    const zombies = await updateZombie.execute(id, input);

    repo.received(1).update(id, input);
    expect(zombies).toStrictEqual(fakeZombie);
  });
});
