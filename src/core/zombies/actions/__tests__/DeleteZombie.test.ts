import Substitute from '@fluffy-spoon/substitute';
import { ZombiesRepository } from '../../domain/ZombiesRepository';
import { DeleteZombie } from '../DeleteZombie';


describe('DeleteZombie Action Unit Test', () => {
  it('should invoke the repo with the right parameters and return the result', async () => {
    const repo = Substitute.for<ZombiesRepository>();
    const id = 'test-id';
    const deleteZombie = new DeleteZombie(repo);

    deleteZombie.execute(id)

    repo.received(1).delete(id);
  });
});
