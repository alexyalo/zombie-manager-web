import Substitute from '@fluffy-spoon/substitute';
import { Zombie } from '../../domain/Zombie';
import { ZombiesRepository } from '../../domain/ZombiesRepository';
import { GetZombies } from '../GetZombies';

const fakeZombies: Zombie[] = [{
  id: 'test-id',
  name: 'John Doe',
  location: {
    id: 'test-id',
    name: 'Hospital'
  }
},
{
  id: 'test-id-2',
  name: 'Jane Doe',
  location: {
    id: 'test-id-2',
    name: 'Hospital'
  }
}];

describe('GetZombies Action Unit Test', () => {
  it('should invoke the repo with no parameters and return the results when there are no parameters', async () => {
    const repo = Substitute.for<ZombiesRepository>();
    repo.find().resolves(fakeZombies);
    const getZombies = new GetZombies(repo);

    const zombies = await getZombies.execute();

    repo.received(1).find();
    expect(zombies).toStrictEqual(fakeZombies);
  });

  it('should invoke the repo with no parameters and return the results when locationId is passed', async () => {
    const repo = Substitute.for<ZombiesRepository>();
    const locationId = 'test-id';
    repo.find({ locationId }).resolves(fakeZombies);
    const getZombies = new GetZombies(repo);

    const zombies = await getZombies.execute(locationId);

    repo.received(1).find({ locationId });
    expect(zombies).toStrictEqual(fakeZombies);
  });
});
