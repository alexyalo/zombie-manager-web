import { Zombie } from "../domain/Zombie";
import { ZombiesRepository } from "../domain/ZombiesRepository";

export class GetZombies {
  constructor(private zombiesRepo: ZombiesRepository) { }

  async execute(locationId?: string): Promise<Zombie[]> {
    return locationId ? this.zombiesRepo.find({locationId}) : this.zombiesRepo.find();
  }
}