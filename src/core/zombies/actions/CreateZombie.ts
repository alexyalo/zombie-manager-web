import { ZombieInput } from "../domain/ZombieInput";
import { Zombie } from "../domain/Zombie";
import { ZombiesRepository } from "../domain/ZombiesRepository";

export class CreateZombie {
  constructor(private zombiesRepo: ZombiesRepository) { }

  execute(input: ZombieInput): Promise<Zombie> {
    return this.zombiesRepo.create(input);
  }
}