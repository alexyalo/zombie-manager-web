import { ZombiesRepository } from "../domain/ZombiesRepository";

export class DeleteZombie {
  constructor(private zombiesRepo: ZombiesRepository) { }

  execute(id: string): Promise<void> {
    return this.zombiesRepo.delete(id);
  }
}