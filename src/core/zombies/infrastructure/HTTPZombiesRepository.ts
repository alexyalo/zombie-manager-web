import { HTTPService } from "../../common/domain/HTTPService";
import { APIConfig } from "../../common/infrastructure/APIConfig";
import { ZombieInput } from "../domain/ZombieInput";
import { Zombie } from "../domain/Zombie";
import { ZombiesRepository } from "../domain/ZombiesRepository";
import { ZombiesResponse } from "./ZombiesResponse";
import { GetZombiesParams } from "../domain/GetZombiesParams";

export class HTTPZombiesRepository implements ZombiesRepository {
  constructor(private httpService: HTTPService, private apiConfig: APIConfig) { }

  async create(input: ZombieInput): Promise<Zombie> {
    const url = `${this.apiConfig.url}/zombies`;
    return this.httpService.post<ZombieInput, Zombie>(url, input);
  }

  async find(params?: GetZombiesParams): Promise<Zombie[]> {
    const queryParams = params?.locationId ? `?locationId=${params.locationId}` : '';
    const url = `${this.apiConfig.url}/zombies${queryParams}`;

    const response = await this.httpService.get<ZombiesResponse>(url);

    return response.data;
  }

  async update(id: string, input: ZombieInput): Promise<Zombie> {
    const url = `${this.apiConfig.url}/zombies/${id}`;
    return this.httpService.patch<ZombieInput, Zombie>(url, input);
  }

  delete(id: string): Promise<void> {
    const url = `${this.apiConfig.url}/zombies/${id}`;
    return this.httpService.delete(url);
  }
}
