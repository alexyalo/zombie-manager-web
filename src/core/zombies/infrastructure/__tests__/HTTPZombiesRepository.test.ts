import Substitute from "@fluffy-spoon/substitute";
import { HTTPService } from "../../../common/domain/HTTPService";
import { ZombieInput } from "../../domain/ZombieInput";
import { Zombie } from "../../domain/Zombie";
import { HTTPZombiesRepository } from "../HTTPZombiesRepository";
import { ZombiesResponse } from "../ZombiesResponse";

const fakeZombiesResponse: ZombiesResponse = {
  data: [{
    id: 'test1',
    name: 'John Doe',
    location: {
      id: 'test2',
      name: 'Hospital'
    }
  }]
};

const fakeZombies: Zombie[] = [{
  id: 'test1',
  name: 'John Doe',
  location: {
    id: 'test2',
    name: 'Hospital'
  },
}];

const fakeURL = 'http://localhost';

describe('HTTPZombiesRepository Unit Test', () => {
  describe('find method', () => {
    it('should invoke HTTPService with the right parameters and parse the results to domain objects', async () => {
      const httpService = Substitute.for<HTTPService>();
      httpService.get(`${fakeURL}/zombies`).resolves(fakeZombiesResponse);
      const repo = new HTTPZombiesRepository(httpService, { url: fakeURL });

      const locations = await repo.find();

      expect(locations).toStrictEqual(fakeZombies);
    });

    it('should invoke HTTPService with the right parameters and parse the results to domain objects when there are params', async () => {
      const locationId = 'test-id';
      const httpService = Substitute.for<HTTPService>();
      httpService.get(`${fakeURL}/zombies?locationId=${locationId}`).resolves(fakeZombiesResponse);
      const repo = new HTTPZombiesRepository(httpService, { url: fakeURL });

      const locations = await repo.find({locationId});

      expect(locations).toStrictEqual(fakeZombies);
    });
  });

  describe('create method', () => {
    it('should invoke HTTPService with the right parameters and parse the results to domain objects', async () => {
      const data: ZombieInput = {name: 'test', locationId: 'test'};
      const httpService = Substitute.for<HTTPService>();
      httpService.post(`${fakeURL}/zombies`, data).resolves(fakeZombiesResponse.data[0]);
      const repo = new HTTPZombiesRepository(httpService, { url: fakeURL });

      const zombie = await repo.create(data);

      expect(zombie).toStrictEqual(fakeZombies[0]);
    });
  });
  
  describe('update method', () => {
    it('should invoke HTTPService with the right parameters and parse the results to domain objects', async () => {
      const id = 'test-id';
      const data: ZombieInput = {name: 'test', locationId: 'test'};
      const httpService = Substitute.for<HTTPService>();
      httpService.patch(`${fakeURL}/zombies/${id}`, data).resolves(fakeZombiesResponse.data[0]);
      const repo = new HTTPZombiesRepository(httpService, { url: fakeURL });

      const zombie = await repo.update(id, data);

      expect(zombie).toStrictEqual(fakeZombies[0]);
    });
  });

  describe('delete method', () => {
    it('should invoke HTTPService with the right parameters', async () => {
      const id = 'test-id';
      const httpService = Substitute.for<HTTPService>();
      const repo = new HTTPZombiesRepository(httpService, { url: fakeURL });
      
      repo.delete(id);
      
      httpService.received(1).delete(`${fakeURL}/zombies/${id}`);
    });
  });
});
