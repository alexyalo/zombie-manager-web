import { Zombie } from "../domain/Zombie";

export interface ZombiesResponse {
  data: Zombie[];
}
