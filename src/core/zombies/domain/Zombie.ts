export interface Zombie {
  id: string;
  name: string;
  location: {
    id: string;
    name: string;
  }
}