import { CreateRepository } from "../../common/domain/repositories/CreateRepository";
import { FindRepository } from "../../common/domain/repositories/FindRepository";
import { ZombieInput } from "./ZombieInput";
import { Zombie } from "./Zombie";
import { UpdateRepository } from "../../common/domain/repositories/UpdateRepository";
import { DeleteRepository } from "../../common/domain/repositories/DeleteRepository";
import { GetZombiesParams } from "./GetZombiesParams";

type Zombies = Zombie[];
export interface ZombiesRepository extends FindRepository<Zombies, GetZombiesParams>, CreateRepository<Zombie, ZombieInput>, UpdateRepository<Zombie, ZombieInput>, DeleteRepository {}