import { APIConfig } from "../core/common/infrastructure/APIConfig";
import { AxiosHTTPService } from "../core/common/infrastructure/AxiosHTTPService";
import { ActionProvider } from "./ActionProvider";
import { RepositoryProvider } from "./RepositoryProvider";

export const getActionProvider = () => {
  const apiConfig: APIConfig = {
    url: process.env.REACT_APP_API_HOST || '',
  }
  const httpService = new AxiosHTTPService();
  const repoProvider = new RepositoryProvider(httpService, apiConfig);
  return new ActionProvider(repoProvider);
}