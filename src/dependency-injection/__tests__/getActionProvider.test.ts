import { APIConfig } from "../../core/common/infrastructure/APIConfig";
import { AxiosHTTPService } from "../../core/common/infrastructure/AxiosHTTPService";
import { ActionProvider } from "../ActionProvider";
import { getActionProvider } from "../getActionProvider";
import { RepositoryProvider } from "../RepositoryProvider";

process.env.REACT_APP_API_HOST = 'http://localhost:8080';

describe('getActionProvider Unit Test', () => {
  it('should return an instance of ActionProvider with everything needed', () => {
    const apiConfig: APIConfig = {
      url: process.env.REACT_APP_API_HOST || '',
    }
    const httpService = new AxiosHTTPService();
    const repoProvider = new RepositoryProvider(httpService, apiConfig);
    const expected = new ActionProvider(repoProvider);

    const actionProvider = getActionProvider();

    expect(actionProvider).toStrictEqual(expected);
  });
});
