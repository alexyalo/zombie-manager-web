import { createContext, useContext } from 'react';
import { ActionProvider } from './ActionProvider';
import { getActionProvider } from './getActionProvider';

interface AppContext {
  actions: ActionProvider;
  showLoading: (value: boolean) => void;
}

export const context: AppContext = {
  actions: getActionProvider(),
  showLoading: () => {},
};

export const Context = createContext(context);
export function useAppContext() {
  return useContext(Context);
}
