import { HTTPService } from "../core/common/domain/HTTPService";
import { APIConfig } from "../core/common/infrastructure/APIConfig";
import { LocationsRepository } from "../core/locations/domain/LocationsRepository";
import { HTTPLocationsRepository } from "../core/locations/infrastructure/HTTPLocationsRepository";
import { ZombiesRepository } from "../core/zombies/domain/ZombiesRepository";
import { HTTPZombiesRepository } from "../core/zombies/infrastructure/HTTPZombiesRepository";

export class RepositoryProvider {
  constructor(private httpService: HTTPService, private apiConfig: APIConfig) { }

  getZombiesRepository(): ZombiesRepository {
    return new HTTPZombiesRepository(this.httpService, this.apiConfig);
  }

  getLocationsRepository(): LocationsRepository {
    return new HTTPLocationsRepository(this.httpService, this.apiConfig);
  }
}