import { GetLocations } from "../core/locations/actions/GetLocations";
import { CreateZombie } from "../core/zombies/actions/CreateZombie";
import { DeleteZombie } from "../core/zombies/actions/DeleteZombie";
import { GetZombies } from "../core/zombies/actions/GetZombies";
import { UpdateZombie } from "../core/zombies/actions/UpdateZombie";
import { RepositoryProvider } from "./RepositoryProvider";

export class ActionProvider {
  constructor(private repoProvider: RepositoryProvider) {}

  forDeleteZombie(): DeleteZombie {
    return new DeleteZombie(this.repoProvider.getZombiesRepository());
  }
  
  forUpdateZombie(): UpdateZombie {
    return new UpdateZombie(this.repoProvider.getZombiesRepository());
  }

  forCreateZombie(): CreateZombie {
    return new CreateZombie(this.repoProvider.getZombiesRepository());
  }

  forGetZombies(): GetZombies {
    return new GetZombies(this.repoProvider.getZombiesRepository());
  }

  forGetLocations(): GetLocations {
    return new GetLocations(this.repoProvider.getLocationsRepository());
  }
}