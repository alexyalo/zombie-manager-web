import React from "react";
import { Location } from "../../../core/locations/domain/Location"
import { LocationCard } from "./LocationCard";

interface Params {
  locations: Location[];
  onClick: (location: Location) => void;
}

export const LocationCards = ({ locations, onClick }: Params) => {
  const mapLocations = () => locations!.map(location => (<LocationCard onClick={onClick} key={`location-card-${location.id}`} location={location} />));

  return (
    <>
      <div className="row mt-4">
        {locations?.length ? mapLocations() : (<h3>No Locations</h3>)}
      </div>
    </>
  );
}
