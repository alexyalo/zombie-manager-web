import { Location } from '../../../core/locations/domain/Location';

interface Params {
  location: Location;
  onClick: (location: Location) => void;
}

export const LocationCard = ({ location, onClick }: Params) => (
  <div className="col col-md-4">
    <div className="card border-secondary mb-3">
      <div className="card-header"><button onClick={() => onClick(location)} className="btn btn-link btn-lg"><i className="fas fa-map-marker-alt"></i> {location.name}</button></div>
      <div className="card-body">
        <h4 className="card-title">Population</h4>
        <p className="card-text">{location.zombieCount}</p>
      </div>
    </div>
  </div>
);