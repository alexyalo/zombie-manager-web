import React from "react"
import { Zombie } from "../../../core/zombies/domain/Zombie"
import { ZombieActions } from "./ZombieActions"

interface Params {
  zombie: Zombie;
  onUpdateClicked: (zombie: Zombie) => void;
  onDeleteClicked: (zombie: Zombie) => void;
}

export const ZombieRow = ({ zombie, onUpdateClicked, onDeleteClicked }: Params) => {
  return (
    <tr>
      <th scope="row">{zombie.name}</th>
      <td>{zombie.location.name}</td>
      <td><ZombieActions onDeleteClicked={onDeleteClicked} onUpdateClicked={onUpdateClicked} zombie={zombie} /></td>
    </tr>
  )
}