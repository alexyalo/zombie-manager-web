export interface ZombieFormData {
  id?: string;
  name: string;
  locationId: string;
}
