import { SyntheticEvent, useState } from "react";
import { useForm } from "react-hook-form";
import { Location } from "../../../core/locations/domain/Location";
import { Zombie } from "../../../core/zombies/domain/Zombie";
import { ZombieFormData } from "./ZombieFormData";

interface Params {
  onSave: (data: ZombieFormData) => void;
  locations: Location[];
  zombie?: Zombie;
}

export const ZombieForm = ({onSave, locations, zombie}: Params) => {
  const getFormOptions = () => {
    if (zombie)
      return {
        defaultValues: {
          name: zombie.name,
          locationId: zombie.location.id,
        },
      };

    return {};
  };
  const { register, handleSubmit } = useForm(getFormOptions());
  const [selectedLocationId, setSelectedLocationId] = useState<string>();

  const setSelectedLocation = (e: SyntheticEvent<HTMLSelectElement, Event>) => {
    setSelectedLocationId(e.currentTarget.value);
  }

  const onSubmit = (data: ZombieFormData) => {
    onSave({
      ...data,
      id: zombie?.id,
    });
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="form-group">
        <label htmlFor="zombie-name">Name</label>
        <input id="zombie-name" ref={register({required: true})} className="form-control" name="name" placeholder="Enter Zombie name" />
      </div>

      <div className="form-group">
        <select ref={register({required: true, minLength: 1})} value={selectedLocationId} onChange={setSelectedLocation} name="locationId" className="custom-select">
          <option value={''}>Select a Location</option>
          { locations.length ? locations.map((l) => <option key={`location-option-${l.id}`} value={l.id}>{l.name}</option>) : (<></>)}
        </select>
      </div>

      <button type="submit" className="btn btn-primary">Submit</button>
    </form>
  )
}