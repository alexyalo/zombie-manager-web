import React, { useEffect, useState } from "react";
import { Location } from "../../../core/locations/domain/Location";
import { Zombie } from "../../../core/zombies/domain/Zombie";
import { useAppContext } from "../../../dependency-injection/context";
import { CreateZombieFormModal } from "./modals/CreateZombieFormModal";
import { DeleteZombieModal } from "./modals/DeleteZombieModal";
import { UpdateZombieFormModal } from "./modals/UpdateZombieFormModal";
import { ZombieRow } from "./ZombieRow";

interface Params {
  locations: Location[];
  selectedLocation?: Location;
  clearSelectedLocation: () => void,
  onSaved: () => void;
}

const CreateZombieButton = ({ onClick }: { onClick: () => void }) => <button type="button" onClick={onClick} className="btn btn-primary mb-2"><i className="fas fa-plus"></i> Create one</button>

export const ZombieList = ({ locations, selectedLocation, onSaved, clearSelectedLocation }: Params) => {
  const { actions, showLoading } = useAppContext();

  const [zombies, setZombies] = useState<Zombie[]>();
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [zombieForUpdate, setZombieForUpdate] = useState<Zombie>();
  const [zombieForDelete, setZombieForDelete] = useState<Zombie>();
  const [updateList, doUpdateList] = useState(false);

  useEffect(() => {
    const fetchZombies = async () => {
      showLoading(true);
      const getZombiesAction = actions.forGetZombies();

      const data = await getZombiesAction.execute(selectedLocation?.id);
      showLoading(false);
      setZombies((_) => data);
    }
    fetchZombies();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [actions, updateList, selectedLocation]);

  const showCreateZombieModal = (): void => {
    setShowCreateModal(true);
  }

  const onUpdateClicked = (zombie: Zombie) => {
    setZombieForUpdate(zombie);
    setShowUpdateModal(true);
  }

  const onDeleteClicked = (zombie: Zombie) => {
    setZombieForDelete(zombie);
    setShowDeleteModal(true);
  }

  const onZombieSaved = () => {
    hideCreateModal();
    doUpdateList(!updateList);
    onSaved();
  }

  const onZombieUpdated = () => {
    hideUpdateModal();
    setZombieForUpdate(undefined);
    doUpdateList(!updateList);
    onSaved();
  }

  const onZombieDeleted = () => {
    hideDeleteModal();
    setZombieForDelete(undefined);
    doUpdateList(!updateList);
    onSaved();
  }

  const hideCreateModal = () => {
    setShowCreateModal(false);
  }

  const hideUpdateModal = () => {
    setShowUpdateModal(false);
  }

  const hideDeleteModal = () => {
    setShowDeleteModal(false);
  }

  const mapZombies = () => zombies!.map((zombie) => <ZombieRow onDeleteClicked={onDeleteClicked} onUpdateClicked={onUpdateClicked} key={`zombie-row-${zombie.id}`} zombie={zombie} />);

  return (
    <>
      <h2 className="mt-2">Zombies{selectedLocation ? ` at ${selectedLocation.name}` : ' in all locations'}</h2>
      <div className="d-flex flex-row-reverse">
        {selectedLocation ? (<button onClick={() => clearSelectedLocation()} className="btn btn-primary mb-2 ml-2">View zombies in all locations</button>) : (<></>)}
        <CreateZombieButton onClick={showCreateZombieModal} />
      </div>

      <table className="table table-hover table-striped">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Current Location</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {zombies?.length ? mapZombies() : (<tr><td colSpan={3}><h4>No zombies yet!</h4> <CreateZombieButton onClick={showCreateZombieModal} /></td></tr>)}
        </tbody>
      </table>
      <CreateZombieFormModal locations={locations} show={showCreateModal} onSaved={onZombieSaved} onHide={hideCreateModal} />
      { zombieForUpdate ? <UpdateZombieFormModal zombie={zombieForUpdate} locations={locations} show={showUpdateModal} onSaved={onZombieUpdated} onHide={hideUpdateModal} /> : (<></>)}
      { zombieForDelete ? <DeleteZombieModal zombie={zombieForDelete} show={showDeleteModal} onDeleted={onZombieDeleted} onHide={hideDeleteModal} /> : (<></>)}
    </>
  );
}