import { Zombie } from "../../../core/zombies/domain/Zombie"

interface Params {
  zombie: Zombie;
  onUpdateClicked: (zombie: Zombie) => void,
  onDeleteClicked: (zombie: Zombie) => void,
}

export const ZombieActions = ({ zombie, onUpdateClicked, onDeleteClicked }: Params) => {
  return (
    <div>
      <button onClick={() => onUpdateClicked(zombie)} className="btn btn-link"><i className="fas fa-pencil-alt"></i></button>
      <button onClick={() => onDeleteClicked(zombie)} className="btn btn-link"><i className="far fa-trash-alt"></i></button>
    </div>
  )
}