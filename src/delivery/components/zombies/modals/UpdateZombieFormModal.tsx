import React from "react"
import { Modal } from "react-bootstrap"
import { Location } from "../../../../core/locations/domain/Location"
import { Zombie } from "../../../../core/zombies/domain/Zombie"
import { useAppContext } from "../../../../dependency-injection/context"
import { ZombieForm } from "../ZombieForm"
import { ZombieFormData } from "../ZombieFormData"

interface Params {
  onSaved: () => void,
  onHide: () => void,
  show: boolean;
  locations: Location[];
  zombie: Zombie;
}
export const UpdateZombieFormModal = ({ zombie, locations, show, onHide, onSaved }: Params) => {
  const { actions, showLoading } = useAppContext();

  const onSave = async (data: ZombieFormData) => {
    showLoading(true);
    const updateZombie = actions.forUpdateZombie();
    await updateZombie.execute(data.id!, {
      name: data.name,
      locationId: data.locationId,
    });
    showLoading(false);
    onSaved();
  }

  return (
    <>
      <Modal
        show={show}
        onHide={onHide}
        size="sm"
        centered
        aria-labelledby="contained-modal-title-vcenter"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Update Zombie
        </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ZombieForm zombie={zombie} locations={locations} onSave={onSave} />
        </Modal.Body>
      </Modal>
    </>
  )
}