import React from "react"
import { Modal } from "react-bootstrap"
import { Zombie } from "../../../../core/zombies/domain/Zombie"
import { useAppContext } from "../../../../dependency-injection/context"

interface Params {
  onDeleted: () => void,
  onHide: () => void,
  show: boolean;
  zombie: Zombie;
}
export const DeleteZombieModal = ({ zombie, show, onHide, onDeleted }: Params) => {
  const { actions,showLoading } = useAppContext();

  const onDelete = async (zombie: Zombie) => {
    showLoading(true);
    const deleteZombie = actions.forDeleteZombie();
    await deleteZombie.execute(zombie.id);
    showLoading(false);
    onDeleted();
  }

  return (
    <>
      <Modal
        show={show}
        onHide={onHide}
        size="sm"
        centered
        aria-labelledby="contained-modal-title-vcenter"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Delete Zombie
        </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Are you sure you want to delete {zombie.name}? It can't be undone.</p>
          <button className="btn btn-primary" onClick={() => onDelete(zombie)}>Confirm</button>
          <button className="btn btn-secondary ml-2" onClick={onHide}>Cancel</button>
        </Modal.Body>
      </Modal>
    </>
  )
}