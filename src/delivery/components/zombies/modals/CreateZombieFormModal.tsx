import React from "react"
import { Modal } from "react-bootstrap"
import { Location } from "../../../../core/locations/domain/Location"
import { ZombieInput } from "../../../../core/zombies/domain/ZombieInput"
import { useAppContext } from "../../../../dependency-injection/context"
import { ZombieForm } from "../ZombieForm"
import { ZombieFormData } from "../ZombieFormData"

interface Params {
  onSaved: () => void,
  onHide: () => void,
  show: boolean;
  locations: Location[];
}
export const CreateZombieFormModal = ({ locations, show, onHide, onSaved }: Params) => {
  const { actions, showLoading } = useAppContext();
  
  const onSave = async (data: ZombieFormData) => {
    showLoading(true);
    const createZombie = actions.forCreateZombie();
    await createZombie.execute(data as ZombieInput);
    showLoading(false);
    onSaved();

  }

  return (
    <>
      <Modal
        show={show}
        onHide={onHide}
        size="sm"
        centered
        aria-labelledby="contained-modal-title-vcenter"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Create Zombie
        </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ZombieForm locations={locations} onSave={onSave} />
        </Modal.Body>
      </Modal>
    </>
  )
}