# Zombie Manager Challenge

We've got the zombies under control, but we need to keep track of them. That's where you come in. We need you to build an app for us. A Zombie Manager.
We can quarantine zombies in three locations: the hospital, the school, and the warehouse. We need the app to keep track of where each zombie is being held, how many zombies are in each location, and we need to be able to move zombies between the locations.
This needs to be a fully-functional app with a separate front-end and back-end. The front-end should communicate with the back-end via an API.
Please use React and submit your code in a Github repo that we can access. And keep track of how long it took you. 

# Website
[https://zombie-manager-web.vercel.app/](https://zombie-manager-web.vercel.app/)

# Unit Tests
All the `core` and `dependency-injection` logic is unit tested. These unit tests can be found inside the `__tests__` folders inside the `core` layer. 
Eg: If you want to see the unit tests for the Action `src/core/zombies/actions/CreateZombie.ts`, you will find it in `src/core/zombies/actions/__tests__/CreateZombie.test.ts`

```
$ yarn test
```

# Start local server

## Environment variables
Before running the server, make sure you create a file named `.env.local` on the root of the project and add this variable to it:
```
REACT_APP_API_HOST=https://zombie-manager-api.herokuapp.com
```

## Start server
```
$ yarn start
```

# Architecture
I applied concepts of Clean Architecture (by Uncle Bob) and Interaction Driven Design (by Sandro Mancuso).
All core domain logic is decoupled from the React framework and can be found inside the `core` folder.
React is the delivery mechanism and our `core` logic knows nothing about it. Inside the `delivery` folder you will find all the React components.

## Core
The `core` folder architecture is grouped by "component", and inside each "component" you will find the use cases (actions), domain interfaces, and infrastructure implementations of those interfaces.

## Reference
If you are not familiar with this type of architecture please check Sandro Mancuso's video in which he explains it: [Crafted Design by Sandro Mancuso](https://vimeo.com/128596005)